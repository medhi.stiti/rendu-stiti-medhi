from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

def encrypt(msg: str, shift: int) -> str:
    unicodes = str_to_unicodes(msg)
    encrypted_unicodes = [(x + shift) % 0x110000 for x in unicodes]
    encrypted_msg = unicodes_to_str(encrypted_unicodes)
    return encrypted_msg

def decrypt(msg: str, shift: int) -> str:
    # le déchiffrement de César est en réalité le même que le chiffrement, mais avec un décalage négatif
    return encrypt(msg, -shift)

def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    for shift in range(0x110000): # on essaye tous les décalages possibles
        decrypted_s = decrypt(s, shift)
        if 'ennemis' in decrypted_s:
            return decrypted_s, shift
    raise RuntimeError("Failed to attack")
