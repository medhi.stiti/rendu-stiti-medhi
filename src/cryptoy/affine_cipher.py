def compute_permutation(a: int, b: int, n: int) -> list[int]:
    result = [(a * i + b) % n for i in range(n)]
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    result = [0]*n
    for i, j in enumerate(perm):
        result[j] = i
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    n = 128  # Taille de l'alphabet Unicode de base
    perm = compute_permutation(a, b, n)
    unicodes = str_to_unicodes(msg)
    encrypted_unicodes = [perm[u] for u in unicodes]
    return unicodes_to_str(encrypted_unicodes)


def encrypt_optimized(msg: str, a: int, b: int) -> str:
    n = 128
    unicodes = str_to_unicodes(msg)
    encrypted_unicodes = [(a * u + b) % n for u in unicodes]
    return unicodes_to_str(encrypted_unicodes)


def decrypt(msg: str, a: int, b: int) -> str:
    n = 128
    inv_perm = compute_inverse_permutation(a, b, n)
    unicodes = str_to_unicodes(msg)
    decrypted_unicodes = [inv_perm[u] for u in unicodes]
    return unicodes_to_str(decrypted_unicodes)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    n = 128
    unicodes = str_to_unicodes(msg)
    decrypted_unicodes = [(a_inverse * (u - b)) % n for u in unicodes]
    return unicodes_to_str(decrypted_unicodes)


def compute_affine_keys(n: int) -> list[int]:
    affine_keys = [i for i in range(1, n) if gcd(i, n) == 1]
    return affine_keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1
    raise RuntimeError(f"{a} has no inverse")

def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    n = 128
    keys = compute_affine_keys(n)
    for a in keys:
        a_inverse = compute_affine_key_inverse(a, keys, n)
        decrypted_msg = decrypt_optimized(s, a_inverse, 58)
        if decrypted_msg.startswith("bombe"):
            return decrypted_msg, (a, 58)
    raise RuntimeError("Failed to attack")

def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    n = 128
    keys = compute_affine_keys(n)
    for a in keys:
        for b in range(n):
            a_inverse = compute_affine_key_inverse(a, keys, n)
            decrypted_msg = decrypt_optimized(s, a_inverse, b)
            if decrypted_msg.startswith("bombe"):
                return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")


