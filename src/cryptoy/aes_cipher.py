from cryptography.hazmat.primitives.ciphers.aead import AESGCM

def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    aesgcm = AESGCM(key)
    ct = aesgcm.encrypt(nonce, msg, None)
    return ct

def decrypt(ct: bytes, key: bytes, nonce: bytes) -> bytes:
    aesgcm = AESGCM(key)
    msg = aesgcm.decrypt(nonce, ct, None)
    return msg
